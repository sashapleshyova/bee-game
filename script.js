let hpElem = document.getElementsByClassName('hp_js')[0];
let energyElem = document.getElementsByClassName('energy_js')[0];
let numberOfFlowersElem = document.getElementsByClassName('number_of_flowers_js')[0];

let startButton = document.getElementsByClassName('start_js')[0];
let restartButton = document.getElementsByClassName('restart_js')[0];

let game = false;


startButton.onclick = function () {
    waterMas = [];
    game = true;
    document.getElementsByClassName('first_alert_js')[0].classList.add('no_active');
};
restartButton.onclick = function () {
    waterMas = [];
    game = true;
    document.getElementsByClassName('game_finish_js')[0].classList.add('no_active');
};


let canvas = document.getElementById('canvas');

canvas.height = 1080;
canvas.width = 1920;

let context = canvas.getContext("2d");

context.beginPath();

let beeToRight = new Image();
beeToRight.src = "img/beeToRight.png";
let beeToLeft = new Image();
beeToLeft.src = "img/beeToLeft.png";
let beeToBottom = new Image();
beeToBottom.src = "img/beeToBottom.png";
let beeToTop = new Image();
beeToTop.src = "img/beeToTop.png";

let flower = new Image();
flower.src = "img/flower.png";

let water = new Image();
water.src = "img/drop.png";

let currentBee = beeToRight;

const HP = 10;
const ENERGY = 1000;

let score = 0;
let energy = ENERGY;
let hp = HP;

let speed = 15;
let beeHeight = 100;
let beeWidth = 100;

let flowerHeight = 50;
let flowerWidth = 50;

let waterHeight = 43;
let waterWidth = 26;

let xBee = canvas.width / 2;
let yBee = canvas.height / 2;

hpElem.innerHTML = hp;
energyElem.innerHTML = energy;
numberOfFlowersElem.innerHTML = score;

function Flower() {
    this.xFlower = Math.random() * (canvas.width - flowerWidth);
    this.yFlower = Math.random() * (canvas.height - flowerHeight);
}

function Water() {
    this.xWater = Math.random() * (canvas.width - waterWidth);
    this.yWater = 0;
    this.crashed = false;
}

let flowers = [new Flower(), new Flower(), new Flower(), new Flower(), new Flower()];
let waterMas = [];

currentBee.onerror = function () {
};
flower.onerror = function () {
};


onload = function () {
    setInterval(function () {
        if (game) {
            hpElem.innerHTML = hp;
            energyElem.innerHTML = energy;
            numberOfFlowersElem.innerHTML = score;

            context.clearRect(0, 0, canvas.width, canvas.height);

            context.drawImage(currentBee, xBee, yBee, beeWidth, beeWidth);

            for (let i = 0; i < flowers.length; i++) {
                if (encounter(beeWidth, beeHeight, xBee, yBee, flowerWidth, flowerHeight, flowers[i].xFlower, flowers[i].yFlower)) {
                    flowers[i].xFlower = Math.random() * canvas.width;
                    flowers[i].yFlower = Math.random() * canvas.height;
                    score++;
                    energy += 50;
                    break;
                }
                context.drawImage(flower, flowers[i].xFlower, flowers[i].yFlower, flowerWidth, flowerHeight);
            }

            for (let i = 0; i < waterMas.length; i++) {
                if (encounter(beeWidth, beeHeight, xBee, yBee, waterWidth, waterHeight, waterMas[i].xWater, waterMas[i].yWater) && !waterMas[i].crashed) {
                    waterMas[i].crashed = true;
                    hp--;
                }

                context.drawImage(water, waterMas[i].xWater, ++waterMas[i].yWater, waterWidth, waterHeight);

                if (waterMas[i].yWater > canvas.height) {
                    waterMas.shift();
                }
            }

            if (hp <= 0 || energy <= 0) {
                hpElem.innerHTML = '0';
                game = false;
                document.getElementsByClassName('game_finish_js')[0].classList.remove('no_active');
                hp = HP;
                score = 0;
                energy = ENERGY;
            }
        }
    }, 5);
};


currentBee.onload = function () {
    document.addEventListener('keydown', function (event) {
        switch (event.keyCode) {
            case 37:
                if (xBee >= 0) {
                    xBee -= speed;
                }
                energy--;
                currentBee = beeToLeft;
                break;
            case 38:
                if (yBee >= 0) {
                    yBee -= speed;
                }
                energy--;
                currentBee = beeToTop;
                break;
            case 39:
                if (xBee + speed + beeWidth <= 1920) {
                    xBee += speed;
                }
                energy--;
                currentBee = beeToRight;
                break;
            case 40:
                if (yBee + speed + beeHeight <= 1080) {
                    yBee += speed;
                }
                energy--;
                currentBee = beeToBottom;
                break;
        }
    });
};

setInterval(function () {
    for (let i = 0; i < flowers.length; i++) {
        flowers[i].xFlower = Math.random() * canvas.width;
        flowers[i].yFlower = Math.random() * canvas.height;
    }
}, 10000);

setInterval(function () {
    waterMas.push(new Water())
}, 300);

context.closePath();

function encounter(firstWidth, firstHeight, firstX, firstY, secondWidth, secondHeight, secondX, secondY) {
    return (secondX > firstX && secondX < firstX + firstWidth || secondX + secondWidth > firstX && secondX + secondWidth < firstX + firstWidth) &&
        (secondY + secondHeight > firstY && secondY + secondHeight < firstY || secondY > firstY && secondY < firstY + firstHeight);
}
